$('.date-own').datepicker({
    format: 'dd-mm-yyyy',
    maxDate:'+1m +10d',
    minDate: new Date(1900, 1, 1)
});

$('.date-own-year').datepicker({
    format: "yyyy",
    viewMode: "years",
    minViewMode: "years",
    yearRange: "1900:2021"
});

