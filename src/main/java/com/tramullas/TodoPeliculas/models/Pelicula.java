package com.tramullas.TodoPeliculas.models;

import java.sql.Timestamp;
import java.util.List;

public class Pelicula {


    private int id;

    private String titulo;

    private int anno;

    private int duracion;

    private String pais;

    private String direccion;

    private String genero;

    private String sinopsis;

    private String portada;

    private Timestamp fechaEstreno;

    private List<Actor> actores;

    public Pelicula(int id, String titulo, int anno, int duracion, String pais, String direccion, String genero, String sinopsis, String portada, Timestamp fechaEstreno) {
        this.id = id;
        this.titulo = titulo;
        this.anno = anno;
        this.duracion = duracion;
        this.pais = pais;
        this.direccion = direccion;
        this.genero = genero;
        this.sinopsis = sinopsis;
        this.portada = portada;
        this.fechaEstreno = fechaEstreno;
    }

    public Pelicula() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public int getAnno() {
        return anno;
    }

    public void setAnno(int anno) {
        this.anno = anno;
    }

    public int getDuracion() {
        return duracion;
    }

    public void setDuracion(int duracion) {
        this.duracion = duracion;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getSinopsis() {
        return sinopsis;
    }

    public void setSinopsis(String sinopsis) {
        this.sinopsis = sinopsis;
    }

    public String getPortada() {
        return portada;
    }

    public void setPortada(String portada) {
        this.portada = portada;
    }

    public Timestamp getFechaEstreno() {
        return fechaEstreno;
    }

    public void setFechaEstreno(Timestamp fechaEstreno) {
        this.fechaEstreno = fechaEstreno;
    }

    public List<Actor> getActores() {
        return actores;
    }

    public void setActores(List<Actor> actores) {
        this.actores = actores;
    }


}
