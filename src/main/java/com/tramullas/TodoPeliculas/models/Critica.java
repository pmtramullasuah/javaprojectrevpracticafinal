package com.tramullas.TodoPeliculas.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.sql.Timestamp;
import java.util.Objects;

public class Critica {

    private int id;

    private int peliculaId;

    private String valoracion;

    private Integer nota;

    @JsonFormat(pattern="dd-MM-yyyy", locale = "es-ES", timezone = "Europe/Madrid")
    @DateTimeFormat(pattern="dd-MM-yyyy")
    private Timestamp fecha;

    private Usuario usuario;

    public Critica() { }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public int getPeliculaId() {
        return peliculaId;
    }

    public void setPeliculaId(int peliculaId) {
        this.peliculaId = peliculaId;
    }


    public String getValoracion() {
        return valoracion;
    }

    public void setValoracion(String valoracion) {
        this.valoracion = valoracion;
    }


    public Integer getNota() {
        return nota;
    }

    public void setNota(Integer nota) {
        this.nota = nota;
    }


    public Timestamp getFecha() {
        return fecha;
    }

    public void setFecha(Timestamp fecha) {
        this.fecha = fecha;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Critica that = (Critica) o;
        return id == that.id && peliculaId == that.peliculaId && Objects.equals(valoracion, that.valoracion) && Objects.equals(nota, that.nota) && Objects.equals(fecha, that.fecha) && Objects.equals(usuario, that.usuario);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, peliculaId, valoracion, nota, fecha, usuario);
    }
}
