package com.tramullas.TodoPeliculas.controllers;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class ExceptionHandlingController {


    @ExceptionHandler({Exception.class})
    public String handleError(HttpServletRequest req, Exception ex, RedirectAttributes attributes) {
        attributes.addFlashAttribute("error", ex.toString());
        return "redirect:/home";
    }

    @ExceptionHandler({NoHandlerFoundException.class})
    public String handleNotFoundError(HttpServletRequest req, Exception ex, RedirectAttributes attributes) {
        attributes.addFlashAttribute("error", ex.toString());
        return "redirect:/home";
    }


}