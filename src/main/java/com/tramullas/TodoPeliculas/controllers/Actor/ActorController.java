package com.tramullas.TodoPeliculas.controllers.Actor;


import com.tramullas.TodoPeliculas.models.Actor;
import com.tramullas.TodoPeliculas.paginator.PageRender;
import com.tramullas.TodoPeliculas.services.Actor.IActorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/actores")
public class ActorController {

    @Autowired
    IActorService actorService;

    @GetMapping("/")
    public String listadoActores(Model model, @RequestParam(name="page", defaultValue="0") int page) {

        List<Actor> actores = actorService.buscarTodos();

        Pageable pageable = PageRequest.of(page, 5);
        Page<Actor> listActores = actorService.pageResults(actores, pageable);
        PageRender<Actor> pageRender = new PageRender<Actor>("/actores/", listActores);

        model.addAttribute("listadoActores", listActores);
        model.addAttribute("page", pageRender);

        return "actor/listActor";

    }

    @GetMapping("/informacion/{id}")
    public String fichaActor(Model model, @PathVariable Integer id) {

        Actor actor = actorService.buscarActorPorId(id);

        model.addAttribute("actor", actor);

        return "actor/viewActor";


    }

    @GetMapping("/nuevo")
    public String formularioActor(Model model) throws ParseException {
        Actor actor = new Actor();
        actor.setFechaNacimiento(new SimpleDateFormat("dd/MM/yyyy").parse("01/01/1900"));
        //Colocamos el limite inferior para la fecha

        model.addAttribute("actor",actor);
        model.addAttribute("msgSubmit","Introducir");
        return "actor/formActor";
    }

    @PostMapping("/guardar")
    public String guardarActor(Model model, Actor actor, RedirectAttributes attributes) {
        actorService.guardarActor(actor);
        attributes.addFlashAttribute("msg", "¡El actor fue insertado correctamente!");
        return "redirect:/actores/";
    }

    @GetMapping("/eliminar/{id}")
    public String eliminarActor(Model model, @PathVariable("id") Integer id, RedirectAttributes attributes) {
        Integer result = actorService.eliminarActor(id);
        String msg = "";
        switch (result) {
            case 200: msg = "Actor borrado correctamente"; break;
            case 400: msg = "Existen peliculas asociadas a este actor y no se puede eliminar"; break;
            default: msg = "No se ha podido borrar el actor, inténtelo más tarde"; break;
        }
        attributes.addFlashAttribute("msg", msg);
        return "redirect:/actores/";
    }

    @GetMapping("/editar/{id}")
    public String editarActor(Model model, @PathVariable("id") Integer id, RedirectAttributes attributes) {
        Actor actor = actorService.buscarActorPorId(id);
        model.addAttribute("actor",actor);
        model.addAttribute("msgSubmit","Guardar");
        return "actor/formActor";
    }

}
