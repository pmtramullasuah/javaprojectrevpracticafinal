package com.tramullas.TodoPeliculas.services.Pelicula;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.tramullas.TodoPeliculas.models.Critica;
import com.tramullas.TodoPeliculas.models.Pelicula;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;


import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Service
public class PeliculaServiceImpl implements IPeliculaService {

    ObjectMapper mapper = new ObjectMapper();

    @Autowired
    RestTemplate template;

    String url = "http://localhost:8090/api/peliculas-actores/peliculas";

    @Override
    public Page<Pelicula> pageResults(List<Pelicula> peliculasList, Pageable pageable) {
        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;
        List<Pelicula> list;

        if (peliculasList.size() < startItem) {
            list = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, peliculasList.size());
            list = peliculasList.subList(startItem, toIndex);
        }

        Page<Pelicula> page = new PageImpl<>(list, PageRequest.of(currentPage, pageSize), peliculasList.size());
        return page;
    }

    @Override
    public List<Pelicula> buscarTodos() {
        Pelicula[] peliculas = template.getForObject(url, Pelicula[].class);
        return Arrays.asList(peliculas);
    }

    @Override
    public List<Pelicula> buscarPorTitulo(String titulo) {
        Pelicula[] peliculas = template.getForObject(url+"/buscarportitulo?titulo="+titulo,Pelicula[].class);
        return Arrays.asList(peliculas);
    }

    @Override
    public List<Pelicula> buscarPorGenero(String genero) {
        Pelicula[] peliculas = template.getForObject(url+"/buscarporgenero?genero="+genero,Pelicula[].class);
        return Arrays.asList(peliculas);
    }

    @Override
    public List<Pelicula> buscarPorActor(String actor) {
        Pelicula[] peliculas = template.getForObject(url+"/buscarporactor?nombre="+actor,Pelicula[].class);
        return Arrays.asList(peliculas);
    }

    @Override
    public Pelicula buscarPeliculaPorId(Integer id) {
        Pelicula pelicula = template.getForObject(url+"/"+String.valueOf(id),Pelicula.class);
        return pelicula;
    }

    @Override
    public Integer guardarPelicula(Pelicula pelicula) {
        try{
            if(pelicula.getId()!=0){
                template.put(url,pelicula);
            }else{
                template.postForObject(url, pelicula, String.class);

            }
            return 200;
        }catch (HttpStatusCodeException e){
            return e.getRawStatusCode();
        }

    }

    @Override
    public Double calcularNota(List<Critica> criticas) {
        Double nota_acum = 0.0;
        for(int i=0; i<criticas.size();i++){
            nota_acum += criticas.get(i).getNota();
        }
        return  nota_acum/criticas.size();
    }

    @Override
    public void eliminarPelicula(Integer id) {
        template.delete(url+"/"+String.valueOf(id));
        return;
    }

    @Override
    public void actualizarListaActores(Integer id, List<Integer> idsActores) {
        ObjectNode nodeRequest = mapper.createObjectNode();
        ArrayNode listActores = mapper.createArrayNode();
        for(int i = 0; i<idsActores.size(); i++) {
            listActores.add(idsActores.get(i));
        }
        nodeRequest.set("listActores",listActores);
        template.postForObject(url+"/"+"actualizaractores/"+String.valueOf(id),nodeRequest, String.class);
        return;
    }

    @Override
    public List<Pelicula> buscarPorFiltrosDinamicos(String genero, String titulo, String nombre_actor) {
        String parameters_url = "";
        if((genero!=null) && (genero!="")){
            parameters_url += ("genero="+genero);
        }

        if ((titulo!=null) && (titulo!="")) {
            if (parameters_url != "") {
                parameters_url += "&";
            }
            parameters_url += ("titulo="+titulo);
        }
        if ((nombre_actor!=null) && (nombre_actor!="")) {
            if (parameters_url != "") {
                parameters_url += "&";
            }
            parameters_url += ("actor="+nombre_actor);
        }
        Pelicula[] peliculas = template.getForObject(url+"/filtrosdinamicos?"+parameters_url, Pelicula[].class);
        return Arrays.asList(peliculas);
    }
}
